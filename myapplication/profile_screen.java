package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class profile_screen extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_screen);


        TextView edfullname = (TextView) findViewById(R.id.full_name);
        TextView edName = (TextView) findViewById(R.id.ed_username);
        TextView edpass = (TextView) findViewById(R.id.ed_password);
        TextView edphone = (TextView) findViewById(R.id.ed_phone);
        TextView edaddress = (TextView) findViewById(R.id.ed_Address);
        TextView eroll = (TextView) findViewById(R.id.ed_roll_no);

        Button btn_exit = (Button) findViewById(R.id.btn_exit);


        edfullname.setText(signup_activity.getValue6());
        edpass.setText(signup_activity.getValue1());
        edName.setText(signup_activity.getValue());
        edphone.setText(signup_activity.getValue3());
        edaddress.setText(signup_activity.getValue2());
        eroll.setText(signup_activity.getValue4());
        
        
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder b = new AlertDialog.Builder(profile_screen.this);
                b.setMessage("EXIT");
                b.setCancelable(true);
                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                b.setPositiveButton("YES!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                AlertDialog dia = b.create();
                dia.show();
            }
        });





    }
}