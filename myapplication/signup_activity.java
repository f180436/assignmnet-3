package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class signup_activity extends AppCompatActivity {

    private static String username;
    public static String getValue() {
        return username;
    }
    private static String fullname;
    public static String getValue6() {
        return fullname;
    }
    private static String password;
    public static String getValue1() {
        return password;
    }
    private static String address;
    public static String getValue2() {
        return address;
    }
    private static String phone;
    public static String getValue3() {
        return phone;
    }
    private static String roll;
    public static String getValue4() {
        return roll;
    }

    private EditText edUsername;
    private EditText edPassword;
    private EditText edConfirmPassword;
    private Button btnCreateUser;
    private EditText edphone;
    private EditText edAddress;
    private EditText edfname;
    private EditText edlname;
    private EditText ed_roll_no1;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        edUsername = findViewById(R.id.ed_username);
        edPassword = findViewById(R.id.ed_password);
        edConfirmPassword = findViewById(R.id.ed_confirm_pass);
        btnCreateUser = findViewById(R.id.btn_create_user);
        edphone = findViewById(R.id.ed_phone);
        edAddress = findViewById(R.id.ed_Address);
        edfname = findViewById(R.id.fname);
        edlname = findViewById(R.id.lname);
        ed_roll_no1 = findViewById(R.id.ed_roll_no);


        btnCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strPassword = edPassword.getText().toString();
                String strUsername = edUsername.getText().toString();
                String strConfirmPassword = edConfirmPassword.getText().toString();
                String strphone = edphone.getText().toString();
                String strAddress = edAddress.getText().toString();
                String strfname = edfname.getText().toString();
                String strlname = edlname.getText().toString();
                String strroll = ed_roll_no1.getText().toString();


                    String full_name1=strfname+" "+strlname;
                    fullname=full_name1;
                    username=strUsername;
                    password=strPassword;
                    phone=strphone;
                    address=strAddress;
                    roll=strroll;
                Intent I=new Intent(signup_activity.this, MainActivity.class);
                startActivity(I);


                    signup_activity.this.finish();






            }
        });



    }



}